import React from 'react';
import BookInfo from './BookInfo';
import crudMethods from '../services/crudMethods';

class StoreApp extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			title: null,
			author: null,
			price: null
		};
	}

	

	render() {
		return (
			<div className="container">
				<BookInfo />
			</div>
		);
	}
}

export default StoreApp;