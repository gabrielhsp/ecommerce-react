import React from 'react';
import crudMethods from '../services/crudMethods';

class IncludeBook extends React.Component {
	constructor(props) {
		super(props);
	}

	handleSubmit(e) {
		e.preventDefault();

		crudMethods.insertBook(this.refs.title.value, this.refs.author.value, this.refs.price.value).then((response) => {
			this.props.
		});
	}

	render() {
		return (
			<div className="app-form-container">
				<h1 className="app-title">Cadastre um livro</h1>

				<form className="app-form-include">
					<input className="app-form-input" id="app-form-input-title" ref="title" type="text" placeholder="Título do livro" />
					<input className="app-form-input" id="app-form-input-author" ref="author" type="text" placeholder="Autor do livro" />
					<input className="app-form-input" id="app-form-input-author" ref="price" type="text" placeholder="Preço do livro" />
					<button className="app-form-submit" type="submit">Cadastrar</button>
				</form>
			</div>
		);
	}
}

export default IncludeBook;