import React from 'react';

const BookInfo = (props) => {
	const bookInfo = props ? (
		<article className="book-item">
			<h2 className="book-title">{props.titulo}</h2>
			<span className="book-author">{props.autor}</span>
			<span className="book-price">{props.preco}</span>
		</article>
	) : null;

	return bookInfo;
};

export default BookInfo;