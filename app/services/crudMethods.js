import axios from 'axios';

const crudMethods = {
	getListBooks(title, author) {
		return axios.get('//lab01.akna.com.br/testes/livros.php');
	},

	insertBook(title, author, price) {
		axios({
			method: 'POST',
			url: '//lab01.akna.com.br/testes/livros.php',
			data: {
				titulo: title,
				autor: author,
				preco: price
			}
		});
	}
};

export default crudMethods;