import React from 'react';
import ReactDOM from 'react-dom';

import StoreApp from './components/StoreApp';

ReactDOM.render(<StoreApp />, document.getElementById('app'));